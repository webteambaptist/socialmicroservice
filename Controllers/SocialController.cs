﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.Extensions.Configuration;
using NLog;
using SocialMicroservice.Models;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SocialMicroservice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SocialController : ControllerBase
    {
        private readonly IConfiguration _config;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private PhysiciansPortalContext ppEntities;
        public SocialController(IConfiguration config, PhysiciansPortalContext context)
        {
            _config = config;
            ppEntities = context;
            var nlogConfig = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") {FileName = $"logs\\SocialMicroservice-{DateTime.Now:MM-dd-yyyy}.log"};
            nlogConfig.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = nlogConfig;
        }
        // GET: api/<SocialController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
         /// <summary>
        /// delete sub for given physician ID
        /// </summary>
        /// <returns></returns>
        [Route("delSub")]
        [HttpGet]
        public ActionResult delSubs()
        {
            var headers = Request.Headers;
            var phyid = ""; //subscription physician ID
            var subid = ""; //subscriber to delete

            try
            {
                if (headers.ContainsKey("phyid"))
                {
                    phyid = headers["phyid"].First();
                }

                if (headers.ContainsKey("subid"))
                {
                    subid = headers["subid"].First();
                }


                if ((phyid == null) || (subid == null))
                {
                    return Ok("Missing Data");
                }

                var feed = ppEntities.SocialFeedMatrices.First(x => x.SubscriptionPhysicianId == phyid && x.SubscribersPhysicianId == subid);
                ppEntities.Remove(feed);
                ppEntities.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                Logger.Info("api/feeds/delSub", ex.Message);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// delete post
        /// </summary>
        /// <returns></returns>
        [Route("delPost")]
        [HttpGet]
        public ActionResult delPost()
        {
            var headers = Request.Headers;
            string postID = "";

            try
            {
                if (headers.ContainsKey("postid"))
                {
                    postID = headers["postid"].First();
                }
                else
                {
                    return Ok("Missing Data");
                }

                if (postID == null)
                {
                    return Ok("Missing Data");
                }
                var post = ppEntities.SocialFeedMains.First(x => x.PostId == int.Parse(postID));
                ppEntities.Remove(post);
                ppEntities.SaveChanges();
                
                return Ok();
            }
            catch (Exception ex)
            {
                Logger.Info("api/feeds/delPost", ex.Message);
                return BadRequest();
            }
        }

        // <summary>
        // get subscribers for a given feed
        // </summary>
        // <returns></returns>
        [Route("selSocial")]
        [HttpGet]
        public ActionResult selSubscribes()
        {
            var headers = Request.Headers;
            var profileEid = ""; //subscription physician ID
            var loggedEchoId = "";
            var subPub = new SubsPubs { Subscribers = new List<Subs>(), Subscriptions = new List<Subs>() };

            try
            {
                var matrix = ppEntities.SocialFeedMatrices;
                //var people = ppEntities.GeneralPhysicianInfos;

                if (headers.ContainsKey("profileEID"))
                {
                    profileEid = headers["profileEID"].First();
                }

                if (profileEid == null)
                {
                    return Ok("Missing Data");
                }

                if (headers.ContainsKey("loggedEchoID"))
                {
                    loggedEchoId = headers["loggedEchoID"].First();
                }

                if (loggedEchoId == null)
                {
                    return Ok("Missing Data");
                }
                try
                {
                    if (matrix.Any(x => x.SubscriptionPhysicianId == profileEid && x.SubscribersPhysicianId == loggedEchoId))
                    {
                        var subList = matrix.First(x =>
                            x.SubscriptionPhysicianId == profileEid && x.SubscribersPhysicianId == loggedEchoId);
                        subPub.Subscribed = subList != null;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Info("api/feeds/selSocial", ex.Message);
                    subPub.Subscribed = false;
                }

                var subscribers = matrix.Where(x => x.SubscriptionPhysicianId == profileEid).ToList();

                foreach (var s in subscribers)
                {
                    try
                    {
                        var u = new Subs
                        {
                            ID = s.Id,
                            SubscriptionPhysicianID = s.SubscriptionPhysicianId,
                            SubscribersPhysicianID = s.SubscribersPhysicianId,
                            SubscribersPhysicianName = getName(s.SubscribersPhysicianId),
                            SubscriptionPhysicianName = getName(s.SubscriptionPhysicianId)
                        };
                        subPub.Subscribers.Add(u);
                    }
                    catch (Exception ex)
                    {
                        Logger.Info("api/feeds/selSocial", ex.Message);
                    }
                }

                var subscriptions = matrix.Where(x => x.SubscribersPhysicianId == profileEid).ToList();

                foreach (var s in subscriptions)
                {
                    try
                    {
                        var u = new Subs
                        {
                            ID = s.Id,
                            SubscriptionPhysicianID = s.SubscriptionPhysicianId,
                            SubscribersPhysicianID = s.SubscribersPhysicianId,
                            SubscribersPhysicianName = getName(s.SubscribersPhysicianId),
                            SubscriptionPhysicianName = getName(s.SubscriptionPhysicianId)
                        };
                        subPub.Subscriptions.Add(u);
                    }
                    catch (Exception ex)
                    {
                        Logger.Info("api/feeds/selSocial", ex.Message);
                    }
                }

                return Ok(subPub);
            }
            catch (Exception ex)
            {
                Logger.Info("api/feeds/selSocial", ex.Message);
                return BadRequest();
            }
        }

        private string getName(string subscribersPhysicianID)
        {
            var people = ppEntities.GeneralPhysicianInfos;

            string name;

            try
            {
                var person = people.First(x => x.Echoid == subscribersPhysicianID);

                if (person == null)
                {
                    return "";
                }
                name = person.FirstName + " " + person.LastName + ", " + person.Credentials;
                return name;
            }
            catch(Exception ex)
            {
                Logger.Info("SocialController/getName", ex.Message);
                return "";
            }
        }

        /// <summary>
        /// get subscribers for a given feed
        /// </summary>
        /// <returns></returns>
        //[Route("selSubs")]
        //[HttpGet]
        //public ActionResult selSubs()
        //{
        //    var headers = Request.Headers;
        //    string phyid = ""; //subscription physician ID

        //    try
        //    {
        //        var matrix = ppEntities.SocialFeedMatrices;

        //        if (headers.ContainsKey("phyid"))
        //        {
        //            phyid = headers["phyid"].First();
        //        }

        //        if (phyid == null)
        //        {
        //            return Ok("");
        //        }
        //        var subList = matrix.First(x => x.SubscriptionPhysicianId == phyid);
        //        return Ok(subList);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Info("api/feeds/selSubs", ex.Message);
        //        return BadRequest();
        //    }
        //}

        /// <summary>
        /// get subscribers for a given feed
        /// </summary>
        /// <returns></returns>
        [Route("insSubs")]
        [HttpPost]
        public ActionResult insSubs([FromBody] SocialFeedMatrix sfm)
        {
            try
            {
                var matrix = new SocialFeedMatrix
                {
                    SubscribersPhysicianId = sfm.SubscribersPhysicianId,
                    SubscriptionPhysicianId = sfm.SubscriptionPhysicianId
                };
                ppEntities.SocialFeedMatrices.Add(matrix);
                ppEntities.SaveChanges();
                
                return Ok();
            }
            catch (Exception ex)
            {
                Logger.Info("api/feeds/insSubs", ex.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// get social posts with attachments
        /// </summary>
        /// <returns></returns>
        [Route("getfeeds")]
        [HttpGet]
        public ActionResult selFeeds()
        {
            var headers = Request.Headers;
            string echoID = "";
            
            try
            {
                var feeds =  ppEntities.SocialFeedMains;

                if (headers.ContainsKey("echoid"))
                {
                    echoID = headers["echoid"].First();
                }

                if (feeds == null)
                {
                    return Ok("");
                }
                else
                {
                    var sds = new List<SocialData>();
                    
                    var sdfeeds = feeds.Where(x => x.Echoid == echoID).ToList();

                    var sdfeedsorder = sdfeeds.OrderByDescending(x => x.PostDate);

                    foreach (var s in sdfeedsorder)
                    {
                        var sd = new SocialData
                        {
                            ID = s.Id,
                            PostID = s.PostId,
                            PostAuthor = s.PostAuthor,
                            PostDate = s.PostDate.ToShortDateString(),
                            PostBody = s.PostBody,
                            CreatedDT = s.CreatedDt.ToShortDateString(),
                            LastUpdateDT = s.LastUpdateDt.ToShortDateString(),
                            ECHOID = s.Echoid,
                            PostAuthorEchoID = s.PostAuthorEchoId,
                            attachments = new List<SocialFeedAttachment>()
                        };

                        //get the attachments
                        var files = ppEntities.SocialFeedAttachments.Where(x => x.PostId == s.PostId).ToList();
                        //var files = ppEntities.selSocialFeed2(s.PostID);

                        foreach (var f in files)
                        {
                            try
                            {
                                var sfa = new SocialFeedAttachment
                                {
                                    CreateDt = f.CreateDt,
                                    Id = f.Id,
                                    PostId = f.PostId,
                                    PostFileName = f.PostFileName
                                };
                                sd.attachments.Add(sfa);
                            }
                            catch (Exception ex)
                            {
                                Logger.Info("api/feeds/getfeeds", ex.Message);
                                continue;
                            }
                        }
                        sds.Add(sd);
                    }
                    return Ok(sds);
                }
            }
            catch (Exception ex)
            {
                Logger.Info("api/feeds/getfeeds", ex.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// get file attachment for given ID
        /// </summary>
        /// <returns></returns>
        [Route("getFile")]
        [HttpGet]
        public ActionResult GetFile()
        {
            var headers = Request.Headers;
            var id = 0;

            try
            {
                if (headers.ContainsKey("ID"))
                {
                    var link = headers["ID"].First();

                    try
                    {
                        id = int.Parse(link);
                    }
                    catch (Exception ex)
                    {
                        Logger.Info("api/feeds/getFile", ex.Message);
                        return BadRequest();
                    }
                }

                var doc = ppEntities.SocialFeedAttachments.First(x => x.Id == id);

                var stream = new MemoryStream(doc.PostAttachment);
                using var reader = new BinaryReader(stream);
                var bytes = reader.ReadBytes((int) stream.Length);

                return Ok(bytes);
            }
            catch (Exception ex)
            {
                Logger.Info("api/feeds/getFile", ex.Message);
                return BadRequest();
            }
        }

        /// <summary>
        /// save social posts with attachments
        /// </summary>
        /// <param name="sd"></param>
        /// <returns></returns>
        [Route("savefeeds")]
        [HttpPost]
        public HttpResponseMessage insFeeds()
        {
           // var httpRequest = Request.Form;
            var headers = Request.Headers;
            var postAuthor = "";
            var postBody = "";
            var echoID = "";
            var postAuthorEchoID = "";

            if (headers.ContainsKey("echoid"))
            {
                echoID = headers["echoid"].First();
            }

            if (headers.ContainsKey("postBody"))
            {
               postBody = headers["postBody"].First();
            }

            if (headers.ContainsKey("postAuthor"))
            {
                postAuthor = headers["postAuthor"].First();
            }

            if (headers.ContainsKey("PostAuthorid"))
            {
                postAuthorEchoID = headers["PostAuthorid"].First();
            }

            //process text side first
            var postID = 0;

            try
            {
                var sd = new SocialFeedMain
                {
                    Id = 0,
                    PostAuthor = postAuthor,
                    PostDate = DateTime.Now,
                    PostBody = postBody,
                    CreatedDt = DateTime.Now,
                    LastUpdateDt = DateTime.Now,
                    Echoid = echoID,
                    PostAuthorEchoId = postAuthorEchoID
                };
                //sd.PostID = s.PostID;
                ppEntities.SocialFeedMains.Add(sd);
                ppEntities.SaveChanges();

                //get last post created by this ECHOID to get related POSTID
                var feeds = ppEntities.SocialFeedMains;
                var posts = feeds.Where(x => x.Echoid == sd.Echoid).ToList();
                postID = posts.Last().PostId;

                if (Request.Form.Files.Count <= 0) return new HttpResponseMessage(HttpStatusCode.OK);
                for (var i = 0; i < (Request.Form.Files.Count); i++)
                {
                    var sfa = new SocialFeedAttachment();
                    sfa.PostId = postID;
                    sfa.CreateDt = DateTime.Now;
                    var file = Request.Form.Files[i];

                    using (var binaryReader = new BinaryReader(file.OpenReadStream()))
                    {
                        sfa.PostAttachment = binaryReader.ReadBytes((int)file.Length);
                        var pos = file.FileName.LastIndexOf("\\", StringComparison.Ordinal) + 1;
                        sfa.PostFileName = file.FileName.Substring(pos, file.FileName.Length - pos);
                    }

                    ppEntities.SocialFeedAttachments.Add(sfa);
                }

                ppEntities.SaveChanges();
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Logger.Info("api/feeds/savefeeds", ex.Message);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

        }

        /// <summary>
        /// update entire post, deletes original attachments and reload them
        /// </summary>
        /// <param name="sd"></param>
        /// <returns></returns>
        [Route("updfeedall")]
        [HttpPost]
        public HttpResponseMessage updFeeds()
        {
            var headers = Request.Headers;
        
            var postBody = "";
            var postId = "";

            if (headers.ContainsKey("postBody"))
            {
                postBody = headers["postBody"].First();
            }

            if (headers.ContainsKey("postid"))
            {
                postId = headers["postid"].First();
            }

            try
            {
                var iPostId = int.Parse(postId); 

                var feeds = ppEntities.SocialFeedMains;
                var post = feeds.FirstOrDefault(x => x.PostId == iPostId);
                if (post != null) post.PostBody = postBody;
                ppEntities.SaveChanges();

                try
                {
                    if (Request.Form.Files.Count <= 0) return new HttpResponseMessage(HttpStatusCode.OK);
                    var attach = ppEntities.SocialFeedAttachments.First(x => x.PostId == iPostId);
                    ppEntities.Remove(attach);
                    ppEntities.SaveChanges();
                }
                catch (Exception e)
                {
                    // not really doing anything here. 
                    // if an exception is thrown then they didn't attach a new file so we won't 
                    // delete the old
                }

                foreach (var f in Request.Form.Files)
                {
                    var sfa = new SocialFeedAttachment {PostId = iPostId, CreateDt = DateTime.Now};

                    using (var binaryReader = new BinaryReader(f.OpenReadStream()))
                    {
                        sfa.PostAttachment = binaryReader.ReadBytes((int)f.Length);
                        sfa.PostFileName = f.FileName;
                    }

                    ppEntities.SocialFeedAttachments.Add(sfa);
                }

                ppEntities.SaveChanges();
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Logger.Info("api/feeds/updfeedall", ex.Message);
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

        }
        
    }
}
