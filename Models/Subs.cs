﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMicroservice.Models
{
    public class Subs
    {
        public int ID { get; set; }
        public string SubscriptionPhysicianID { get; set; }
        public string SubscribersPhysicianID { get; set; }
        public string SubscriptionPhysicianName { get; set; }
        public string SubscribersPhysicianName { get; set; }
    }
}
