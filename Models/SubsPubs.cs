﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMicroservice.Models
{
    public class SubsPubs
    {
        public bool Subscribed { get; set; }
        public List<Subs> Subscribers { get; set; }
        public List<Subs> Subscriptions { get; set; }
    }
}
