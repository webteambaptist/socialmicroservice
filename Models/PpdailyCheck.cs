﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SocialMicroservice.Models
{
    public partial class PpdailyCheck
    {
        public int Id { get; set; }
        public string Ntid { get; set; }
        public DateTime LastUpdateDate { get; set; }
    }
}
