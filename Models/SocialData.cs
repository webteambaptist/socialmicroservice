﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialMicroservice.Models
{
    public class SocialData
    {
        public int ID { get; set; }
        public int PostID { get; set; }
        public string PostAuthor { get; set; }
        public string PostDate { get; set; }
        public string PostBody { get; set; }
        public string CreatedDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string ECHOID { get; set; }
        public List<SocialFeedAttachment> attachments { get; set; }
        public string PostAuthorEchoID { get; set; }
    }
}
